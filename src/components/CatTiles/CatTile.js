import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "../../styles/CatTile/CatTile.module.scss";

export default class CatTile extends Component {
  state = {
    active: false,
    hover: false
  };

  onClick = e => {
    e.preventDefault();
    if (!this.props.disabled) {
      this.setState({ active: !this.state.active });
    }
  };

  render() {
    const { foodType, info, weight, description } = this.props;
    const { active, hover } = this.state;

    let tileStyle;
    let textUnder;
    let subHeading = { text: "Сказочное заморское яство", class: null };
    if (active) {
      if (hover) {
        tileStyle = styles.catTileSelectedHover;
        subHeading.text = "Котэ не одобряет?";
        subHeading.class = "text-selected";
      } else {
        tileStyle = styles.catTileSelected;
      }
      textUnder = <React.Fragment>{description}</React.Fragment>;
    } else {
      if (hover) {
        tileStyle = styles.catTileHover;
      } else {
        tileStyle = styles.catTile;
      }
      textUnder = (
        <React.Fragment>
          Чего сидишь? Порадуй котэ,
          <button className={styles.buy} onClick={this.onClick}>
            <span>купи</span>.
          </button>
        </React.Fragment>
      );
    }

    if (this.props.disabled) {
      tileStyle = styles.catTileDisabled;
      textUnder = (
        <React.Fragment>Печалька, с {foodType} закончился.</React.Fragment>
      );
    }

    return (
      <div
        className={tileStyle}
        onClick={this.onClick}
        onMouseEnter={() => this.setState({ hover: false })}
        onMouseLeave={() => this.setState({ hover: true })}
      >
        <p className={subHeading.class}>{subHeading.text}</p>
        <h2>
          Нямушка
          <br />
          <span>с {foodType}</span>
        </h2>
        <ul>
          {info.map((infoItem, index) => (
            <li key={index}>
              {infoItem.amount ? <strong>{infoItem.amount} </strong> : null}
              {infoItem.text}
            </li>
          ))}
        </ul>
        <div className={styles.weightLabel}>
          {weight}
          <span>кг</span>
        </div>
        <p className={styles.textUnder} onClick={e => e.stopPropagation()}>
          {textUnder}
        </p>
      </div>
    );
  }
}

CatTile.propTypes = {
  foodType: PropTypes.string.isRequired,
  info: PropTypes.array.isRequired,
  weight: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};
