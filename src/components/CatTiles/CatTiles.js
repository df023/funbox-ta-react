import React, { Component } from "react";
import CatTile from "./CatTile";
import styles from "../../styles/CatTile/CatTiles.module.scss";

export default class CatTiles extends Component {
  state = {
    tiles: [
      {
        foodType: "фуа-гра",
        info: [{ amount: 10, text: "порций" }, { text: "мышь в подарок" }],
        weight: "0,5",
        description: "Печень утки разварная с артишоками.",
        disabled: false
      },
      {
        foodType: "рыбой",
        info: [
          { amount: 40, text: "порций" },
          { amount: 2, text: "мыши в подарок" }
        ],
        weight: "2",
        description: "Головы щучьи с чесноком да свежайшая сёмгушка.",
        disabled: false
      },
      {
        foodType: "курой",
        info: [
          { amount: 100, text: "порций" },
          { amount: 5, text: "мышей в подарок" },
          { text: "заказчик доволен" }
        ],
        weight: "5",
        description: "Филе из цыплят с трюфелями в бульоне.",
        disabled: true
      }
    ]
  };

  render() {
    const { tiles } = this.state;

    return (
      <div className={styles.catTiles}>
        {tiles.map((tile, index) => (
          <CatTile
            key={index}
            foodType={tile.foodType}
            info={tile.info}
            weight={tile.weight}
            description={tile.description}
            disabled={tile.disabled}
          />
        ))}
      </div>
    );
  }
}
