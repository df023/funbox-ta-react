import React, { Component } from "react";
import "./styles/style.scss";
import CatTiles from "./components/CatTiles/CatTiles";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Ты сегодня покормил кота?</h1>
        <CatTiles />
      </div>
    );
  }
}
